

e3-prlpcb
======

European Spallation Source ERIC Site-specific EPICS module : prlpcb

This module provides device support for the PRL pressure control box, which provides a state machine for this system for controlling the valves. 

Dependencies 
============ 
sequencer <version>


Building the module 
=================== 
This module should compile as a regular E3 module. For instructions on how to build using E3 see the E3 documentation.
* Check if `configure/RELEASE` or `configure/RELEASE.local` reflects your E3 environment
* Run `make rebuild` from the command line.
