#
#  Copyright (c) 2019 - 2020    European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# Author  : Luciano Carneiro Guedes
# email   : luciano.carneiroguedes@ess.eu
# Date    : 2022-01-24
# version : 1.0.0 
#


## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



############################################################################
#
# Add any required modules here that come from startup scripts, etc.
#
############################################################################

REQUIRED += sequencer
sequencer_VERSION=$(SEQUENCER_DEP_VERSION)
REQUIRED += keller33x
keller33x_VERSION=$(KELLER33X_DEP_VERSION)
############################################################################
#
# If you want to exclude any architectures:
#
############################################################################

EXCLUDE_ARCHS += linux-ppc64e6500


############################################################################
#
# Relevant directories to point to files
#
############################################################################

APP:=PRLPCBApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src
#APPCMDS:=$(APP)/cmds


############################################################################
#
# Add any files that should be copied to $(module)/Db
#
############################################################################

TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.template)
TEMPLATES += $(wildcard $(APPDB)/*.substitutions)

# USR_INCLUDES += -I$(where_am_I)$(APPSRC)


############################################################################
#
# Add any files that need to be compiled (e.g. .c, .cpp, .st, .stt)
#
############################################################################

USR_CXXFLAGS += -std=c++11

SOURCES += $(APPSRC)/PressurePRL_snl.st
DBDS += $(APPSRC)/PRLPCB.dbd

############################################################################
#
# Add any header files that should be included in the install (e.g. 
# StreamDevice or asyn header files that are used by other modules)
#
############################################################################

#HEADERS   += 


############################################################################
#
# Add any startup scripts that should be installed in the base directory
#
############################################################################

SCRIPTS += $(wildcard ../iocsh/*.iocsh)
SCRIPTS += $(wildcard ../hw/*.iocsh)
SCRIPTS += $(wildcard ../hw/plc/*.plc)


############################################################################
#
# If you have any .substitution files, then you should probably comment the
# following db target and uncomment the one that follows.
#
############################################################################

.PHONY: 

# USR_DBFLAGS += -I . -I ..
# USR_DBFLAGS += -I $(EPICS_BASE)/db
# USR_DBFLAGS += -I $(APPDB)
# 
# SUBS=$(wildcard $(APPDB)/*.substitutions)
# TMPS=$(wildcard $(APPDB)/*.template)
# 
# # 
# # 	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
# 	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
# 	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db -S $@  > $(basename $(@)).db.d
# 	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db -S $@
# 
# # 	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
# 	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
# 	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db $@  > $(basename $(@)).db.d
# 	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db $@
# 
#
# 

vlibs:

.PHONY: vlibs
