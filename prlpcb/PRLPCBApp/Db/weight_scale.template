record(ai, "$(P)$(R)Weight_Raw-R") {
    field(DESC, "Raw weight of scale")
    field(EGU,  "kg")
    field(PREC, "3")
    field(VAL,  "0")

    info(ARCHIVE_THIS, "VAL")
}

record(ai, "$(P)$(R)Weight-R") {
    field(DESC, "Weight of scale")
    field(EGU,  "kg")
    field(PREC, "3")
    field(VAL,  "0")

    info(ARCHIVE_THIS, "VAL")
}

record(longin, "$(P)$(R)Weight_Percentage-R") {
    field(DESC, "Weight Percentage")
    field(EGU,  "%")
    field(LOPR, "0")
    field(HOPR, "100")
    field(HIHI, "90")
    field(HIGH, "80")
    field(LOW,  "20")
    field(LOLO, "10")
    field(HHSV, "NO_ALARM")
    field(HSV,  "NO_ALARM")
    field(LSV,  "MINOR")
    field(LLSV, "MAJOR")
    field(VAL,  "0")

    info(ARCHIVE_THIS, "VAL")
}

# Calibration Records
record(ao, "$(P)$(R)Weight_Offset-S") {
    field(DESC, "Weight Offset")
    field(EGU,  "kg")
    field(PREC, "3")

    info(autosaveFields, "VAL")
}

record(ao, "$(P)$(R)Weight_Bottle-S") {
    field(DESC, "Bottle Weight")
    field(EGU,  "kg")
    field(PREC, "3")

    info(autosaveFields, "VAL")
}

record(ao, "$(P)$(R)Weight_Init-S") {
    field(DESC, "Weight Initi value")
    field(EGU,  "kg")
    field(PREC, "3")
    field(VAL,  "1")

    info(autosaveFields, "VAL")
}

record(bo, "$(P)$(R)Weight_Tare-S") {
    field(DESC, "Weight Calibration")
    field(VAL,  "0")
    field(ZNAM, "Calibrated")
    field(ONAM, "Processing")
    field(HIGH, "0.1")
}

record(bo, "$(P)$(R)Weight_BottleChangeS") {
    field(DESC, "Weight Calibration")
    field(VAL,  "0")
    field(ZNAM, "Done")
    field(ONAM, "Processing")
    field(HIGH, "0.1")
}

record(calcout, "$(P)$(R)#Weight_Calc") {
    field(CALC, "B?0:(-A)")
    field(INPA, "$(P)$(R)Weight_Raw-R PP")
    field(INPB, "$(P)$(R)Weight_Tare-S CPP")
    field(OUT,  "$(P)$(R)Weight_Offset-S PP")
}

record(calcout, "$(P)$(R)#Weight_Calc2") {
    field(CALC, "(1-(A-B)/A)*100")
    field(INPA, "$(P)$(R)Weight_Init-S PP")
    field(INPB, "$(P)$(R)Weight-R CPP")
    field(OUT,  "$(P)$(R)Weight_Percentage-R PP")
}

record(calcout, "$(P)$(R)#Weight_Calc4") {
    field(CALC, "A")
    field(INPA, "$(P)$(R)Weight_RBV PP")
    field(INPB, "$(P)$(R)Weight_BottleChange CPP")
    field(OUT,  "$(P)$(R)Weight_Init PP")
}

# Process Records
record(waveform, "$(P)$(R)Weight_Cmd") {
    field(DESC, "Set Array to be sent to the device ")
    field(NELM, "4")
    field(INP,  [27, 48, 49, 65])
    field(FTVL, "DOUBLE")
    field(PINI, "YES")
    field(SCAN, "1 second")
}

record(scalcout, "$(P)$(R)#Weight_Parse") {
    field(CALC, "DBL(AA-'\\x1b01A01   '-'1   '-'\\r')")
    field(INAA, "$(INWAV1) CPP")
    field(OUT,  "$(P)$(R)Weight_Raw-R PP")
}

record(calcout, "$(P)$(R)Weight_Calc3") {
    field(DESC, "description")
    field(CALC, "A+B-C")
    field(INPA, "$(P)$(R)Weight_Raw-R CP")
    field(INPB, "$(P)$(R)Weight_Offset-S CPP")
    field(INPC, "$(P)$(R)Weight_Bottle-S CPP")
    field(OUT,  "$(P)$(R)Weight-R PP")
}

record(acalcout, "$(P)$(R)#Weight_ArrayCalc") {
    field(CALC, "AA")
    field(INAA, "$(P)$(R)Weight_Cmd CPP")
    field(OUT,  "$(OUTWAV1) PP")
    field(NELM, "4")
}

