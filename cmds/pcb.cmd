##############################################################################
## EtherCAT Motion Control Phase Reference Line Temperature Control Box IOC configuration
##############################################################################

require ecmccfg, 7.0.1
require std 3.6.2
require calc
require essioc
require prlpcb

## Initiation:
epicsEnvSet("ECMC_VER" ,"7.0.1")
epicsEnvSet("NAMING", "ESSnaming")
epicsEnvSet("IOC" ,"$(IOC="PRL:Ctrl-ECAT-001")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=$(ECMC_VER="7.0.1"),stream_VER=2.8.22,EC_RATE=200,ECMC_ASYN_PORT_MAX_PARAMS=10000"

################################################################################
########Set EtherCAT bus startup timeout command added (defaults to 30seconds):

ecmcConfigOrDie "Cfg.SetEcStartupTimeout(200)"

##############################################################################
# Configure hardware:

epicsEnvSet("ECMC_SLAVE_NUM",95)
epicsEnvShow("ECMC_SLAVE_NUM")

epicsEnvSet("TCB_NUM",1)
epicsEnvShow("TCB_NUM")

ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "$(ECMC_SLAVE_NUM)+1")
##### Load PCB
$(SCRIPTEXEC) $(prlpcb_DIR)/ecmcPCB.iocsh
epicsEnvSet("P","PRL:")
epicsEnvSet("R_PCB","RFS-PRLPCB-001:")
iocshLoad("$(prlpcb_DIR)/pressure_sequencer.iocsh", "P=$(P), R1=RFS-PRLGSV-001:, R2=RFS-PRLGSV-002:, PR_MON1=$(P)$(R_PCB)Press1, PR_MON2=$(P)$(R_PCB)Press2, VALVE1=$(IOC):#m0s097-BO01, VALVE2=$(IOC):#m0s097-BO02, R=$(R_PCB)")

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

##############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"
#ecmcConfigOrDie "Cfg.SetDiagAxisIndex(1)"
ecmcConfigOrDie "Cfg.SetDiagAxisFreq(2)"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

# go active
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)

iocInit()



