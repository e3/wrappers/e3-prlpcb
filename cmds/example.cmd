require pressureprl

epicsEnvSet("P","PRL:")
epicsEnvSet("R","RFS-PRLGSB-001:")

epicsEnvSet("DELAY_A","10")
epicsEnvSet("DELAY_B","20")
epicsEnvSet("DELAY_C","20")
epicsEnvSet("THOLD","20")
epicsEnvSet("PRESS_INIT_S","1.3")

dbLoadRecords("sim.template", "P=$(P), R=$(R)")
dbLoadRecords("sim.template", "P=$(P), R=$(R)2")

iocshLoad("$(E3_CMD_TOP)/../iocsh/pressure_sequencer.iocsh", "P=$(P), R=$(R), PR_MON=$(P)$(R)PV, VALVE=$(P)$(R)PV3, DELAY_A=$(DELAY_A), DELAY_B=$(DELAY_B), DELAY_C=$(DELAY_C), THOLD=$(THOLD), PRESS_INIT_S=$(PRESS_INIT_S)")

afterInit('seq program_snl, "P=$(P), R=$(R), R1=$(R)"')
afterInit('seq program_snl, "P=$(P), R=$(R)2, R1=$(R)"')
iocInit()



